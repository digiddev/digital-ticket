// import a libary to help create a component
import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import reducers from './src/reducers';

import Router from './src/Router';
import { Header, Spinner } from './src/components/common';
import LoginForm from './src/components/LoginForm/LoginForm';
import TicketListOld from './src/components/TicketListOld/TicketListOld';
// import LibraryList from './src/components/LibraryList/LibraryList';

const store = createStore(reducers, {}, (
  applyMiddleware(ReduxThunk)
));

// Create a component
class App extends Component {
  state = { loggedIn: null }

componentWillMount() {
  firebase.initializeApp({
      apiKey: 'AIzaSyCOZR3rP7nXKothL-RdIAuzciy7dsKnglo',
      authDomain: 'digital-key-50009.firebaseapp.com',
      databaseURL: 'https://digital-key-50009.firebaseio.com',
      projectId: 'digital-key-50009',
      storageBucket: 'digital-key-50009.appspot.com',
      messagingSenderId: '61745329962'
  });

  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      this.setState({ loggedIn: true });
    } else {
      this.setState({ loggedIn: false });
    }
  });
}

renderContent() {
  switch (this.state.loggedIn) {
    case true:
      return ( 
        <TicketListOld />
      );
    case false:
      return <LoginForm />;
    default:
      return <Spinner size="large" />;
  }
}

render() {
  return (
    <Provider store={store}>
      <View style={{ flex: 1 }}>
        {/* <Header headerText={'Digital Key'} /> */}
        {/* <LibraryList /> */}
        {/* {this.renderContent()} */}
        <Router />
      </View>
    </Provider>
    );
  }
}

// Render it to the device
export default App;

