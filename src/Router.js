import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import LoginForm from './components/LoginForm/LoginForm';
import TicketList from './components/Ticket/TicketList';
import TicketCreate from './components/Ticket/TicketCreate';
import TicketEdit from './components/Ticket/TicketEdit';

const RouterComponent = () => {
    return (
        <Router>
            <Scene key="root" hideNavBar>
            <Scene key="auth">
                <Scene key="login" component={LoginForm} title="Please Login" initial />
            </Scene>
            <Scene key="main">
                <Scene 
                    rightTitle="Add"
                    onRight={() => Actions.ticketCreate()}
                    key="ticketList" 
                    component={TicketList} 
                    title="Tickets" 
                />
                <Scene key="ticketCreate" component={TicketCreate} title="Create Ticket" />
                <Scene key="ticketEdit" component={TicketEdit} title="Edit Ticket" />
            </Scene>
            </Scene>
        </Router>
    );
};

export default RouterComponent;
