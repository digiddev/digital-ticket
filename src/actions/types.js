export const SELECT_LIBRARY = 'select_library';
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

export const EMPLOYEE_UPDATE = 'employee_update';
export const EMPLOYEE_CREATE = 'employee_create';
export const EMPLOYEES_FETCH_SUCCESS = 'employees_fetch_success'; 
export const EMPLOYEE_SAVE_SUCCESS = 'employee_save_success';

export const TICKET_UPDATE = 'ticket_update';
export const TICKET_CREATE = 'ticket_create';
export const TICKETS_FETCH_SUCCESS = 'tickets_fetch_success'; 
export const TICKET_SAVE_SUCCESS = 'ticket_save_success';
