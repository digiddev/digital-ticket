import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import {
    TICKET_UPDATE,
    TICKET_CREATE,
    TICKETS_FETCH_SUCCESS,
    TICKET_SAVE_SUCCESS
} from './types';

export const ticketUpdate = ({ prop, value }) => {
    return {
        type: TICKET_UPDATE,
        payload: { prop, value }
    };
};


export const ticketCreate = ({ name, date, location, ticket_number }) => {
    const { currentUser } = firebase.auth();

return (dispatch) => {
   firebase.database().ref(`/users/${currentUser.uid}/tickets`)
    .push({ name, date, location, ticket_number })
    .then(() => {
        dispatch({ type: TICKET_CREATE });
        Actions.pop();
    });
    };
};

export const ticketsFetch = () => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/tickets`)
        .on('value', snapshot => {
            dispatch({ type: TICKETS_FETCH_SUCCESS, payload: snapshot.val() });
        });
    };
};

export const ticketSave = ({ name, date, location, ticket_number, uid }) => {
 const { currentUser } = firebase.auth();

 return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/tickets/${uid}`)
    .set({ name, date, location, ticket_number, })
    .then(() => {
        dispatch({ type: TICKET_SAVE_SUCCESS });
        Actions.ticketList({ type: 'reset' });
    });
 };
};

export const ticketDelete = ({ uid }) => {
    const { currentUser } = firebase.auth();

    return () => {
        firebase.database().ref(`/users/${currentUser.uid}/tickets/${uid}`)
        .remove()
        .then(() => {
            Actions.ticketList({ type: 'reset' });
        });
    };
};
