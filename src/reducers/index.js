import { combineReducers } from 'redux';
import LibraryReducer from './LibraryReducers/LibraryRuducer';
import SelectionReducer from './LibraryReducers/SelectionReducer';
import AuthReducer from './AuthReducers/AuthReducers';
import EmployeeFormReducer from './EmployeeFormReducer/EmployeeFormReducer';
import EmployeeReducer from './EmployeeReducer/EmployeeReducer';
import TicketFormReducer from './TicketFormReducer/TicketFormReducer';
import TicketReducer from './TicketReducer/TicketReducer';

export default combineReducers({
    libraries: LibraryReducer,
    selectedLibraryId: SelectionReducer,
    auth: AuthReducer,
    employeeForm: EmployeeFormReducer,
    employees: EmployeeReducer,
    ticketForm: TicketFormReducer,
    tickets: TicketReducer
});
