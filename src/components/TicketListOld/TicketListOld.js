import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';
import firebase from 'firebase';
import TicketDetailOld from './TicketDetailOld';
import { CardSection, Button } from '../common';

class TicketList extends Component {
    state = { albums: [] };

    componentWillMount() {
        axios.get('https://rallycoding.herokuapp.com/api/music_albums')
        .then(response => this.setState({ albums: response.data }));
    }

    renderFirebase() {
        return (
        <CardSection>
            <Button onPress={() => firebase.auth().signOut()}>
                Log Out
            </Button>
        </CardSection>
        );
    }

    renderTickets() {
        return this.state.albums.map(album =>
        <TicketDetailOld key={album.title} album={album} />
        );
    }


    render() {
        return (
                <ScrollView>
                    {this.renderFirebase()}
                    {this.renderTickets()}
                </ScrollView>
            );
        }
    }

export default TicketList;
