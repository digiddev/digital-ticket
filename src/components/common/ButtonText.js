import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const ButtonText = ({ onPress, children }) => {
    const { buttonContainer } = styles;

        return (
        <TouchableOpacity onPress={onPress} style={buttonContainer}>
            <Text>
                {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 300,
        borderRadius: 30,
      },
};


    export { ButtonText };
