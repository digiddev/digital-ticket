import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children }) => {
    const { buttonContainer, loginButton, loginText } = styles;

        return (
        <TouchableOpacity onPress={onPress} style={[buttonContainer, loginButton]}>
            <Text style={loginText}>
                {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 300,
        borderRadius: 30,
      },
      loginButton: {
        backgroundColor: '#00b5ec',
      },
      loginText: {
        color: 'white',
      },
};


    export { Button };
