
import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
// import Communications from 'react-native-communications';
import TicketForm from './TicketForm';
import { ticketUpdate, ticketSave, ticketDelete } from '../../actions';
import { Card, CardSection, Button, Confirm } from '.././common';

class TicketEdit extends Component {
    state = { showModal: false }
    componentWillMount() {
        _.each(this.props.ticket, (value, prop) => {
          this.props.ticketUpdate({ prop, value });
        });
      }

    onButtonPress() {
        const { name, date, location, ticket_number } = this.props;
        this.props.ticketSave({ name, date, location, ticket_number, uid: this.props.ticket.uid });
    }

    // onTextPress() {
    //     const { phone, shift } = this.props;

    //     Communications.text(phone, `your shift is on  ${shift}`);
    // }
    
    onAccept() {
        const { uid } = this.props.ticket;

        this.props.ticketDelete({ uid });
    }

    onDecline() {
        this.setState({ showModal: false });
    }

    render() {
        return (
            <Card>
                <TicketForm />

                <CardSection>
                    <Button onPress={this.onButtonPress.bind(this)}>
                        Save Changes
                    </Button>
                </CardSection>

                {/* <CardSection>
                    <Button onPress={this.onTextPress.bind(this)}>
                        Transfer Ticket
                    </Button>
                </CardSection> */}

                <CardSection>
                    <Button onPress={() => this.setState({ showModal: !this.state.showModal })}>
                        Delete Ticket
                    </Button>
                </CardSection>

                <Confirm
                visible={this.state.showModal}
                onAccept={this.onAccept.bind(this)}
                onDecline={this.onDecline.bind(this)}
                >
                    Are you sure you wanna delete
                </Confirm>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    const { name, date, location, ticket_number, } = state.ticketForm;

    return { name, date, location, ticket_number };
};

export default connect(mapStateToProps, { 
    ticketUpdate,
    ticketSave,
    ticketDelete 
})(TicketEdit);
