import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ticketUpdate, ticketCreate } from '../../actions';
import { Card, CardSection, Button } from '../common';
import TicketForm from './TicketForm';

class TicketCreate extends Component {
    onButtonPress() {
        const { name, date, location, ticket_number } = this.props;

        this.props.ticketCreate({ name, date, location, ticket_number });
    }
    render() {
        console.log(this.props.ticket);
        return (
            <Card>
                <TicketForm {...this.props} />
                <CardSection>
                    <Button onPress={this.onButtonPress.bind(this)}>
                        Create
                    </Button>     
                </CardSection>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
    const { name, date, location, ticket_number } = state.ticketForm;

    return { name, date, location, ticket_number };
};

export default connect(mapStateToProps, { ticketUpdate, ticketCreate })(TicketCreate);
