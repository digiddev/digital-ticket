import React, { Component } from 'react';
import { Text, TouchableWithoutFeedback, View, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { CardSection } from '../common';


class TicketListItem extends Component {
    onRowPress() {
        Actions.ticketEdit({ ticket: this.props.tickets });
    }
    render() {
        const name = this.props.name;
        const { headerContentStyle, thumbnailContainerStyle, thumbnailStyle, titleStyle } = styles;

        return (
            <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
                <View style={headerContentStyle}>
                    <CardSection>
                    <View style={thumbnailContainerStyle}>
                    <Image
                        style={thumbnailStyle}
                        source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                    />
                    </View>
                    <View style={headerContentStyle}>
                    <Text style={titleStyle}>
                        {name}
                    </Text>
                    </View>
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = {
    titleStyle: {
        fontSize: 18,
        paddingLeft: 15
    },
    headerContentStyle: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#fff'
    },
    thumbnailStyle: {
        height: 50,
        width: 50
    },
    thumbnailContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
};

export default TicketListItem;
