import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FlatList } from 'react-native';
import { ticketsFetch } from '../../actions';
import TicketListItem from './TicketListItem';

class TicketList extends Component {
  componentDidMount() {
    this.props.ticketsFetch();
  }
    
    renderRow(tickets) {
      return <TicketListItem tickets={tickets} />;
    }

    renderItem({ item }) {
      return <TicketListItem tickets={item} name={item.name} />;
    }

    render() {
      const tickets = this.props.tickets;
      console.log(this.props.tickets);
        return (
            <FlatList 
              data={tickets}
              renderItem={this.renderItem}
            />
        );
    }
}


const mapStateToProps = state => {
  const tickets = _.map(state.tickets, (val, uid) => {
    return { ...val, uid };
  });

  return { tickets };
};

export default connect(mapStateToProps, { ticketsFetch })(TicketList);

