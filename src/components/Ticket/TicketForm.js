import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import DatePicker from 'react-native-datepicker';
import { ticketUpdate } from '../../actions';
import { CardSection, Input } from '../common';

class TicketForm extends Component {
    constructor(props) {
        super(props);
        this.state = { date: new Date() };
      }
      
    render() {
        return (
        <View>
            <CardSection> 
                    <Input 
                        label="Name"
                        placeholder="Jane"
                        value={this.props.name}
                        onChangeText={value => this.props.ticketUpdate({ prop: 'name', value })}
                    /> 
            </CardSection>

            <CardSection>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={styles.pickerTextStyle}>Date</Text>
                    <DatePicker
                        style={{ width: 200, height: 50 }}
                        date={this.props.date}
                        mode="date"
                        placeholder="select date"
                        format="DD-MM-YYYY"
                        // minDate="2016-05-01"
                        // maxDate="2100-06-01"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                        dateIcon: {
                            width: 0,
                            height: 0,
                        },
                        dateInput: {
                            marginLeft: 36
                        }
                        // ... You can check the source to find the other keys.
                        }}
                        value={this.props.date}
                        onDateChange={value => this.props.ticketUpdate({ prop: 'date', value })}
                    />
                    </View>
            </CardSection>  

            <CardSection>
                <Input 
                    label="Location"
                    placeholder="The Forum"
                    value={this.props.location}
                    onChangeText={value => this.props.ticketUpdate({ prop: 'location', value })}
                />     
            </CardSection>

            <CardSection>
                <Input 
                    label="ticket_number"
                    placeholder="0001"
                    value={this.props.ticket_number}
                    onChangeText={value => this.props.ticketUpdate({ prop: 'ticket_number', 
                    value 
                    })}
                />     
            </CardSection> 
        </View>
        );
    }
}

const styles = {
    pickerTextStyle: {
        fontSize: 18,
        paddingLeft: 20,
        width: 90, 
        height: 50,
    }
};

const mapStateToProps = (state) => {
    const { name, date, location, ticket_number } = state.ticketForm;

    return { name, date, location, ticket_number };
};

export default connect(mapStateToProps, { ticketUpdate })(TicketForm);
